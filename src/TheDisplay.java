import java.util.ArrayList;

public class TheDisplay {
    private int columns;
    private int rows;
    TheRobot robot;
    private String lastSymbol;
    private ArrayList<Gems> gems;
    private ArrayList<ArrayList<String>> theBoard;

    public TheDisplay(int columns, int rows) {
        this.columns = columns;
        this.rows = rows;
        theBoard = new ArrayList<ArrayList<String>>();
        gems = new ArrayList<>();
        this.robot = new TheRobot(0, 0);
    }

    public ArrayList<ArrayList<String>> createBoard() {
        for (int row = 0; row < rows; row++) {
            theBoard.add(new ArrayList<>());
            for (int col = 0; col < columns; col++) {
                theBoard.get(row).add("*");
            }
        }
        return theBoard;
    }

    public void createGems(int numberOfGems) {
        if (numberOfGems < (columns * rows)) {
            for (int i = 0; i < numberOfGems; i++) {
                boolean isOk = false;
                while (!isOk) {
                    int randomColumn = getARandomColumn();
                    int randomRow = getARandomRow();
                    if (randomColumn != 0 || randomRow != 0) {
                        if (gems.isEmpty()) {
                            gems.add(new Gems(randomColumn, randomRow));
                            isOk = true;
                        } else {
                            while (!gems.contains(new Gems(randomColumn, randomRow))) {
                                gems.add(new Gems(randomColumn, randomRow));
                                isOk = true;
                            }
                        }
                    }
                }
            }

        } else {
            System.out.println("Number of gems greater than number of spots");
        }
    }

    public void placeGems() {
        for (int g = 0; g < gems.size(); g++) {
            for (int row = 0; row < rows; row++) {
                for (int col = 0; col < columns; col++) {
                    if (gems.get(g).getRowNumb() == row && gems.get(g).getColumnNumb() == col && gems.get(g).isVisilbe() == true && !(row == robot.getRowNumb() && col == robot.getColumnNumb())) {
                        theBoard.get(row).set(col, gems.get(g).getGemSymbol());
                    }
                }
            }
        }

    }

    private int getARandomColumn() {
        return (int) (Math.random() * columns);
    }

    private int getARandomRow() {
        return (int) (Math.random() * rows);
    }

    public void displayTheBoard() {
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < columns; col++) {
                System.out.print(theBoard.get(row).get(col));
            }
            System.out.println();
        }
    }

    public void displayGems() {
        for (int i = 0; i < gems.size(); i++) {
            gems.get(i).toString();
        }
    }

    public void initializeTheRobot() {
        this.theBoard.get(0).set(0, robot.getRobot());
    }

    public ArrayList<ArrayList<String>> getTheBoard() {
        return theBoard;
    }

    public void updateRobotPosition() {
        //new position
        this.theBoard.get(this.robot.getRowNumb()).set(this.robot.getColumnNumb(), "X");
        //last position
        this.theBoard.get(this.robot.getLastRow()).set(this.robot.getLastCol(), "*");
        if (isItAGem()) {
            this.theBoard.get(this.robot.getRowNumb()).set(this.robot.getColumnNumb(), "O");
        }
    }

    public boolean isItAGem() {
        boolean bool = false;
        for (int i = 0; i < gems.size(); i++) {
            if (gems.get(i).equals(new Gems(robot.getColumnNumb(), robot.getRowNumb()))) {
                gems.get(i).setVisilbe(true);
                bool = true;
            }
        }
        return bool;
    }

    public boolean isAWinner() {
        int counter = 0;
        for (int i = 0; i < gems.size(); i++) {
            if (gems.get(i).isVisilbe()) {
                counter++;
            }
        }
        return (counter == gems.size());
    }

    public TheRobot getRobot() {
        return robot;
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }
}
