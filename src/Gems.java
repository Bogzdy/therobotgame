import java.util.Objects;

public class Gems {
    private int columnNumb;
    private int rowNumb;
    private String gemSymbol = "o";
    private boolean isVisilbe;

    public Gems(int columnNumb, int rowNumb) {
        this.columnNumb = columnNumb;
        this.rowNumb = rowNumb;
        isVisilbe = false;
    }

    public int getColumnNumb() {
        return columnNumb;
    }

    public int getRowNumb() {
        return rowNumb;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gems gems = (Gems) o;
        return columnNumb == gems.columnNumb &&
                rowNumb == gems.rowNumb;
    }

    @Override
    public int hashCode() {
        return Objects.hash(columnNumb, rowNumb);
    }

    public String toString() {
        System.out.println("This gems is on row " + rowNumb + " and on column " + columnNumb);
        return "This gems is on row " + rowNumb + " and on column " + columnNumb;
    }

    public void setColumnNumb(int columnNumb) {
        this.columnNumb = columnNumb;
    }

    public void setRowNumb(int rowNumb) {
        this.rowNumb = rowNumb;
    }

    public String getGemSymbol() {
        return gemSymbol;
    }

    public void setGemSymbol(String gemSymbol) {
        this.gemSymbol = gemSymbol;
    }

    public boolean isVisilbe() {
        return isVisilbe;
    }

    public void setVisilbe(boolean visilbe) {
        isVisilbe = visilbe;
    }
}
