public class Validator {
    public boolean isAKeyWord(String str) {
        str.toLowerCase();
        String[] arr = str.split(" ");
        if (arr[0].equals("up") || arr[0].equals("down") || arr[0].equals("right") || arr[0].equals("left")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean hasSpace(String str) {
        if (!str.matches("(.*)[\\s](.*)")) {
            printError();
            return false;
        } else {
            return true;
        }
    }

    public boolean hasNumber(String str) {
        if (!str.matches("(.*)[0-9](.*)")) {
            printError();
            return false;
        } else {
            return true;
        }
    }

    public void printError() {
        System.out.println("Your directions and steps are not correct!");
    }

}
