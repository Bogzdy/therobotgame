import java.util.Scanner;

public class Manager {
    private Scanner scanner = new Scanner(System.in);
    private TheDisplay display;
    private int numberOfGems;
    private Validator validator = new Validator();

    public Manager(int col, int rows) {
        this.display = new TheDisplay(col, rows);
    }

    public void start() {
        System.out.println("Game has started");
        System.out.print("How many gems(max = " + ((display.getColumns() * display.getRows()) - 1) + "):");
        numberOfGems = scanner.nextInt();
        initDisplay();
        scanner.nextLine();
        boolean isRunning = false;
        int moves = 0;
        while (!isRunning) {
            String input = returnInput();
            String direction = getDirection(input);
            int steps = getSteps(input);
            boolean moveTheRobot = display.getRobot().moveTheRobot(direction, steps, display.getTheBoard());
            while (!moveTheRobot) {
                if (direction.equals("") | steps == -1) {
                    System.out.println("Use as direction \"right\", \"left\", \"up\" or \"down\" followed by a white space and the number of steps you want.");
                }
                input = returnInput();
                direction = getDirection(input);
                steps = getSteps(input);
                moveTheRobot = display.getRobot().moveTheRobot(direction, steps, display.getTheBoard());
            }
            display.updateRobotPosition();
            display.placeGems();
            display.displayTheBoard();
            moves++;
            if (display.isAWinner()) {
                System.out.println("Congratulations! You found all gems in " + moves + " moves.");
                isRunning = true;
            }

        }
    }

    private String returnInput() {
        System.out.print("Insert direction: (ex: down 5)");
        String str = scanner.nextLine();
        return str;
    }

    private void initDisplay() {
        display.createBoard();
        display.createGems(numberOfGems);
        display.placeGems();
        display.initializeTheRobot();
        display.displayTheBoard();
    }

    private String getDirection(String str) {
        String[] direction = str.split(" ");
        if (validator.hasNumber(str) && validator.hasSpace(str) && validator.isAKeyWord(str)) {
            return direction[0].toLowerCase();
        } else {
            return "";
        }
    }

    private int getSteps(String str) {
        String[] steps = str.split(" ");
        if (validator.hasNumber(str) && validator.hasSpace(str) && validator.isAKeyWord(str)) {
            return Integer.parseInt(steps[steps.length - 1]);
        } else {
            return -1;
        }
    }
}
