import java.util.ArrayList;

public class TheRobot {
    private int columnNumb;
    private int rowNumb;
    private int lastCol;
    private int lastRow;
    private final String robot = "X";
    private Validator validator;

    public TheRobot(int columnNumb, int rowNumb) {
        this.columnNumb = columnNumb;
        this.rowNumb = rowNumb;
        lastCol = columnNumb;
        lastRow = rowNumb;
        validator = new Validator();
    }

    public boolean moveTheRobot(String direction, int steps, ArrayList<ArrayList<String>> theBoard) {
        direction.toLowerCase();
        boolean bool = false;
        lastCol = columnNumb;
        lastRow = rowNumb;
        if (direction == "" || steps == -1){
            return false;
        }

        switch (direction) {
            case "left":
                if (steps <= columnNumb) {
                    columnNumb = columnNumb + setLeftRight(direction) * steps;
                    bool = true;
                } else {
                    System.out.println("You can move left only " + columnNumb + " steps.");
                }
                break;
            case "right":
                if (steps + columnNumb < theBoard.get(0).size() && steps >= 0) {
                    columnNumb = columnNumb + setLeftRight(direction) * steps;
                    bool = true;
                } else {
                    System.out.println("You can move right only " + (theBoard.get(0).size() - columnNumb - 1) + " steps.");
                }

                break;
            case "up":
                if (steps <= rowNumb) {
                    rowNumb = rowNumb + setUpDown(direction) * steps;
                    bool = true;
                } else {
                    System.out.println("You can move up only " + rowNumb + " steps.");
                }
                break;
            case "down":
                if (steps + rowNumb < theBoard.size() && steps >= 0) {
                    rowNumb = rowNumb + setUpDown(direction) * steps;
                    bool = true;
                } else {
                    System.out.println("You can move down only " + (theBoard.size() - rowNumb - 1) + " steps");
                }
                break;
        }
        return bool;
    }

    private int setUpDown(String upDown) {
        upDown.toLowerCase();
        int result = 0;
        switch (upDown) {
            case "up":
                result = -1;
                break;
            case "down":
                result = 1;
                break;
        }
        return result;
    }

    private int setLeftRight(String leftRight) {
        leftRight.toLowerCase();
        int result = 0;
        switch (leftRight) {
            case "left":
                result = -1;
                break;
            case "right":
                result = 1;
                break;
        }
        return result;
    }

    public int getColumnNumb() {
        return columnNumb;
    }

    public void setColumnNumb(int columnNumb) {
        this.columnNumb = columnNumb;
    }

    public int getRowNumb() {
        return rowNumb;
    }

    public void setRowNumb(int rowNumb) {
        this.rowNumb = rowNumb;
    }

    public String getRobot() {
        return robot;
    }

    public int getLastCol() {
        return lastCol;
    }

    public int getLastRow() {
        return lastRow;
    }

    @Override
    public String toString() {
        return "TheRobot{" +
                "columnNumb=" + columnNumb +
                ", rowNumb=" + rowNumb +
                ", lastCol=" + lastCol +
                ", lastRow=" + lastRow +
                '}';
    }
}
